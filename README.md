# Solvoyo DevOps Recruitment

## Assignment

One of our developers wants you to create a .Net Core with Docker and CI/CD pipeline for it.
Also CI/CD must have rollback step if something went wrong developers can able to run the rollback.


##### For this task we want you to use/create;
Dockerfile for .Net Core Project

CI/CD: GitLab CI (you can create a GitLab account and use CI/CD for free)

Deployment environment: AWS Fargate

## Links

- [AWS Cloud Development Kit (AWS CDK)](https://github.com/aws/aws-cdk)
- [AWS Well-Architected](https://aws.amazon.com/architecture/well-architected/)
- [Containers](https://www.docker.com/resources/what-container)
- [AWS Fargate Documantation](https://docs.aws.amazon.com/AmazonECS/latest/developerguide/AWS_Fargate.html)